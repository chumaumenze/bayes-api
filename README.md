# Bayesports API

## Summary

Bayes is a simple CRUD app.

## Requirements

+ python>=3.6
+ [pipenv](https://pipenv.readthedocs.io)
+ [Docker Compose](https://docs.docker.com/compose)
+ Postgres database
+ Gunicorn


## Run

0. Set Enivronment Variables

    ```bash
    ecport \
    SECRET_KEY=your_secret_key \
    DOCKER_DB_USER=your_database_username \
    DOCKER_DB_PASS=your_database_password \
    DOCKER_DB_NAME=your_database_name
    ```
1. Run the server:

    ```bash
    docker-compose -f ./docker-compose.yml up -d --build
    ```

2. Navigate to [localhost:5000/api/v1](http://localhost:5000/api/v1)


## Documentation

+ [Bayes API v1 Dcoumentation][documentation]


## Live Deployments

+ TBD


[compose_config]: ./docker-compose.yml
[dockerfile]: ./Dockerfile
[documentation]: https://documenter.getpostman.com/view/2645031/S17nVAix
