FROM python:3.7

WORKDIR /app

COPY Pipfile Pipfile.lock /app/
COPY instance/config.sample.py /app/instance/config.py

RUN pip install pipenv && \
    pipenv install --system --deploy --ignore-pipfile

COPY . /app/

EXPOSE 5000
