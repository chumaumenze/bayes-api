from application import create_app
from application.models import db

from application.utils.request_response_helpers import make_success_response


application = create_app()

with application.app_context():
    db.Model.metadata.reflect(db.engine)
    db.create_all()


@application.route("/", methods=["GET"], strict_slashes=False)
def api_test():
    return make_success_response(
        response_data={
            "message": "Welcome to Bayes Sports API! "
                       "See https://gitlab.com/bayesports/bayes-api for "
                       "documentation"
        }
    )


if __name__ == "__main__":
    host = application.config.setdefault("HOST_IP", "0.0.0.0")
    port = application.config.setdefault("HOST_PORT", 5000)
    prefix = application.config["API_PREFIX"]

    print(f"API running at http://{host}:{port}{prefix}")  # noqa

    application.run(host=host, port=port)
