from datetime import datetime

from flask import g, request
from application.utils.logger import logger

PAYLOADABLE_HTTP_METHODS = ["DELETE", "PATCH", "POST", "PUT"]
SUPPORTED_HTTP_METHODS = PAYLOADABLE_HTTP_METHODS + ["GET", "OPTIONS"]


def current_request_time():
    try:
        return g.current_request_time
    except AttributeError:
        g.current_request_time = datetime.now()
        return current_request_time()


def current_user():
    return g.user


def current_user_id():
    try:
        return g.user.id
    except AttributeError:
        return None


def get_request_ip():
    return (request.access_route[-1] or request.environ.get(
        "HTTP_X_REAL_IP", request.remote_addr))
