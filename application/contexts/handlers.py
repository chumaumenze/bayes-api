from functools import wraps
import json

from flask import g, request

from application.utils.helpers import generate_unique_reference
from application.utils.logger import logger

from . import get_request_ip


def add_cors_support(f):
    allowed_methods = ", ".join(["GET", "POST", "PUT", "OPTIONS", "DELETE"])

    @wraps(f)
    def decorated_func(*args, **kwargs):
        response = f(*args, **kwargs)

        response.headers["Access-Control-Allow-Origin"] = (
            request.headers.get("Origin", "*")
        )
        response.headers["Access-Control-Allow-Credentials"] = "true"
        response.headers["Access-Control-Allow-Methods"] = allowed_methods
        response.headers["Access-Control-Allow-Headers"] = (
            request.headers.get("Access-Control-Request-Headers",
                                "Authorization")
        )

        return response

    return decorated_func


def before_every_request():
    """Do some necessary setup before handling any request."""
    g.api_ref = generate_unique_reference(size=128)
    g.request_ip = get_request_ip()
    g.endpoint = request.endpoint
    g.request_method = request.method
    g.request_data = request.get_data(as_text=True)


@add_cors_support
def after_every_request(response):
    """Do necessary operations after every request."""
    try:
        response_data = json.loads(response.response[0])
    except (KeyError, IndexError):
        response_data = response.response

    request_data = request.json

    logger.info(f"{request.url} | {request_data} | {response_data}")
    return response
