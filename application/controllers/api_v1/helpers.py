import datetime

from application import errors
from application.contexts import current_user
from application.models import User, Series, Tournament

EXPECTED_DATE_FORMAT = "%d-%m-%Y"


def parse_date_string(date_string, format=EXPECTED_DATE_FORMAT):
    try:
        return datetime.datetime.strptime(date_string, format)
    except (TypeError, ValueError):
        raise errors.InvalidRequestData(f"Date formats should be {format}")


def validate_tournament_period(game_series_start_date, game_series_end_date,
                               tournament_start_date, tournament_end_date):
    is_valid_start_date = (tournament_start_date >= game_series_start_date and
                           tournament_start_date <= game_series_end_date)
    is_valid_end_date = (tournament_end_date <= game_series_end_date and
                         tournament_end_date >= game_series_start_date)
    if not (is_valid_start_date and is_valid_end_date):
        raise errors.BadRequest("Tournament start & end date must be within "
                                "the series period.")


def get_series_by_id(id):
    series = Series.get(id=id)
    if series is None:
        raise errors.ResourceNotFound("Series does not exist!")
    return series


def get_tournament_by_id(id):
    tournament = Tournament.get(id=id)
    if tournament is None:
        raise errors.ResourceNotFound("Tournament does not exist!")
    return tournament


def get_user_by_username(username):
    user = User.get(username=username)
    if user is None:
        raise errors.UserNotFound
    return user


def create_user(username, email, password, **user_details):
    """Create a new user"""
    new_user = User(username=username, email=email, password=password, **user_details)
    new_user.save(_raise=errors.UserAlreadyExist)
    return new_user


def update_user(username, **user_details):
    current_active_user = current_user()

    user = get_user_by_username(username)
    if user.username != current_active_user.username:
        raise errors.Forbidden

    user.update(**user_details)
    return user


def reset_password(username, password):
    user = get_user_by_username(username)
    user.update(password=password)

    return user


def create_tournament(name, city, country, start_date, end_date, series_id):
    start_date = parse_date_string(start_date)
    end_date = parse_date_string(end_date)
    game_series = get_series_by_id(series_id)

    validate_tournament_period(game_series.start_date, game_series.end_date,
                               start_date, end_date)

    new_tournament = Tournament(
        name=name,
        city=city,
        country=country,
        start_date=start_date,
        end_date=end_date,
        series_id=series_id
    )
    new_tournament.save()
    return new_tournament


def update_tournament(tournament_id, **tournament_details):
    tournament = get_tournament_by_id(tournament_id)
    start_date = parse_date_string(tournament_details.get("start_date"))
    end_date = parse_date_string(tournament_details.get("end_date"))

    series_id = tournament_details.get("series_id")
    if series_id is not None:
        game_series = get_series_by_id(series_id)
        validate_tournament_period(game_series.start_date, game_series.end_date,
                                   start_date, end_date)

    if start_date is not None:
        tournament_details["start_date"] = start_date

    if end_date is not None:
        tournament_details["end_date"] = end_date

    tournament.update(**tournament_details)
    return tournament


def create_series(name, start_date, end_date):
    start_date = parse_date_string(start_date)
    end_date = parse_date_string(end_date)

    new_game_series = Series(
        name=name,
        start_date=start_date,
        end_date=end_date
    )
    new_game_series.save()
    return new_game_series


def update_game_series(series_id, **series_details):
    game_series = get_series_by_id(series_id)
    start_date = series_details.get("start_date")
    end_date = series_details.get("end_date")

    if start_date is not None:
        series_details["start_date"] = parse_date_string(start_date)

    if end_date is not None:
        series_details["end_date"] = parse_date_string(end_date)

    game_series.update(**series_details)
    return game_series
