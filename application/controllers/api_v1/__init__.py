from datetime import timedelta

from flask import Blueprint
from flask_jwt_extended import create_access_token

from application.contexts import current_user
from application.constants import TOKEN_SECONDS_VALID

api_v1 = Blueprint("api_v1", __name__)

from .routes import *


@api_v1.route("/", methods=["GET"], strict_slashes=False)
def api_v1_test():
    """Return API connectivity test response"""
    return make_success_response(
        response_data={"message": "Welcome to Bayes Sports API v1!"}
    )


@api_v1.route("/authentication", methods=["GET"])
@authentication_required(_for_token=True)
def authenticate():
    """Authenticate a user"""
    authenticated_user = current_user()

    token_expiration_delta = timedelta(seconds=TOKEN_SECONDS_VALID)
    token = create_access_token(identity=authenticated_user.username,
                                expires_delta=token_expiration_delta)

    data = {
        "token": token,
        "expires_in": TOKEN_SECONDS_VALID,
        "user": authenticated_user.as_json()
    }

    return make_success_response(response_data=data)
