from functools import wraps

from flask import g, current_app, request
from jwt import PyJWTError, ExpiredSignatureError
from flask_jwt_extended import get_jwt_identity, verify_jwt_in_request
from flask_jwt_extended.exceptions import JWTExtendedException

from application import errors
from application.models import User
from application.utils.logger import logger


def make_api_auth():
    api_key = request.headers.get("BAYES_KEY")

    if api_key is None:
        raise errors.BadRequest("Missing authentication credentials.")

    user = User.get(api_key=api_key)
    if user is None:
        raise errors.UnsuccessfulAuthentication

    return user


def make_basic_auth():
    auth = request.authorization
    if not auth:
        raise errors.BadRequest("Missing authentication credentials.")

    user = User.get(username=auth.username)
    if user is not None:
        if user.verify_password(auth.password):
            return user

    logger.error("Username or password authentication failed.")
    raise errors.UnsuccessfulAuthentication


def make_bearer_auth():
    try:
        verify_jwt_in_request()

    except ExpiredSignatureError:
        raise errors.TokenExpired

    except JWTExtendedException as err:
        logger.critical("JWTExtendedException: {}".format(err), exc_info=True)
        raise errors.InvalidToken

    except PyJWTError as err:
        logger.critical("PyJWTError: {}".format(err), exc_info=True)
        raise errors.InvalidToken

    return User.get(username=get_jwt_identity())


def _authenticate_user(_for_token):
    if _for_token:
        try:
            user = make_basic_auth()
        except errors.BadRequest:
            user = make_api_auth()
    else:
        user = make_bearer_auth()

    if not user.is_active:
        raise errors.UnauthorizedUser

    return user


def authentication_required(_for_token=False):
    """Do User authentication.

    Use different authentication strategies to
    determine the authorization of the accessing User
    """

    def view_func_decor(view_func):

        @wraps(view_func)
        def decorated_func(*args, **kwargs):
            app = current_app
            auth_requirement_disabled = app.config.get("NO_AUTHENTICATION")

            if auth_requirement_disabled:
                logger.warn(f"Skipping Authentications!")
                user = User.get(id=1)
            else:
                user = _authenticate_user(_for_token=_for_token)

            g.user = user
            return view_func(*args, **kwargs)

        return decorated_func

    return view_func_decor


def request_data_required(*required_data):
    """Check if request body contains [valid] request data"""

    def decorated_func(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            data = request.json
            if required_data:
                missing_data = set(required_data) - set((data or {}).keys())
                if missing_data:
                    raise errors.InvalidRequestData(
                        f"Required payload: {tuple(missing_data)}")

            return f(*args, **kwargs)

        return wrapper

    return decorated_func
