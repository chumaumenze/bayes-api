from flask import request

from application.utils.request_response_helpers import make_success_response

from .. import api_v1
from ..helpers import create_user, reset_password, update_user
from ..wrappers import request_data_required, authentication_required


@api_v1.route("/users", methods=["PUT"])
@request_data_required("username", "email", "password")
def register_new_user():
    """Register new users"""
    user_details = {
        "email": request.json["email"],
        "username": request.json["username"],
        "password": request.json["password"],
        "first_name": request.json.get("first_name"),
        "last_name": request.json.get("last_name"),
        "address": request.json.get("address")
    }

    user = create_user(**user_details)
    return make_success_response(response_data=user.as_json())


@api_v1.route("/users/<string:username>", methods=["POST"])
@authentication_required()
@request_data_required("username", "email")
def update_a_user(username):
    """Register new users"""
    user_details = {
        "username": username,
        "email": request.json.get("email"),
        "first_name": request.json.get("first_name"),
        "last_name": request.json.get("last_name"),
        "address": request.json.get("address")
    }

    user = update_user(**user_details)
    return make_success_response(response_data=user.as_json())


@api_v1.route("/users/<string:username>/reset-password", methods=["POST"])
@authentication_required()
@request_data_required("password")
def reset_user_password(username):
    """Reset a user password"""
    password = request.json["password"]

    user = reset_password(username, password)
    return make_success_response(response_data=user.as_json())
