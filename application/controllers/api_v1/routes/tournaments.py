from flask import request

from application.models import Tournament
from application.utils.request_response_helpers import make_success_response

from .. import api_v1
from ..helpers import create_tournament, update_tournament, get_tournament_by_id
from ..helpers import parse_date_string
from ..wrappers import request_data_required, authentication_required


@api_v1.route("/tournaments", methods=["GET"])
@authentication_required()
def get_all_tournaments():
    """Get all tournaments"""
    start_date = request.args.get("start_date")
    end_date = request.args.get("end_date")
    series_id = request.args.get("series_id")

    base_query = Tournament.query

    if start_date is not None:
        start_date = parse_date_string(start_date)
        base_query =base_query.filter(Tournament.start_date == start_date)

    if end_date is not None:
        end_date = parse_date_string(end_date)
        base_query =base_query.filter(Tournament.end_date == end_date)

    if series_id is not None:
        base_query = base_query.filter(Tournament.series_id == series_id)

    tournaments = [t.as_json() for t in base_query.all()]
    return make_success_response(response_data=tournaments)


@api_v1.route("/tournaments/<tournament_id>", methods=["GET"])
@authentication_required()
def get_single_tournament(tournament_id):
    """Get single tournament"""
    tournament = get_tournament_by_id(tournament_id)
    return make_success_response(response_data=tournament.as_json())


@api_v1.route("/tournaments", methods=["PUT"])
@authentication_required()
@request_data_required(*["name", "city", "country", "start_date", "end_date", "series_id"])
def add_tournament():
    """Add a tournament"""
    tournament = create_tournament(
        name=request.json["name"],
        city=request.json["city"],
        country=request.json["country"],
        start_date=request.json["start_date"],
        end_date=request.json["end_date"],
        series_id=request.json["series_id"]
    )
    return make_success_response(response_data=tournament.as_json())


@api_v1.route("/tournaments/<tournament_id>", methods=["POST"])
@authentication_required()
@request_data_required(*["name", "city", "country", "start_date", "end_date", "series_id"])
def update_single_tournament(tournament_id):
    """Update a tournament"""
    tournament = update_tournament(
        tournament_id=tournament_id,
        name=request.json["name"],
        city=request.json["city"],
        country=request.json["country"],
        start_date=request.json["start_date"],
        end_date=request.json["end_date"],
        series_id=request.json["series_id"]
    )
    return make_success_response(response_data=tournament.as_json())


@api_v1.route("/tournaments/<string:tournament_id>", methods=["DELETE"])
@authentication_required()
def delete_tournament(tournament_id):
    """Delete a tournament"""
    tournament = get_tournament_by_id(tournament_id)
    tournament.delete()

    return make_success_response(response_data=None)
