from flask import request

from application.models import Series
from application.utils.request_response_helpers import make_success_response

from .. import api_v1
from ..helpers import get_series_by_id, create_series, update_game_series
from ..wrappers import request_data_required, authentication_required


@api_v1.route("/series", methods=["GET"])
@authentication_required()
def get_all_series():
    """Get all game series"""
    series = [t.as_json() for t in Series.query.all()]
    return make_success_response(response_data=series)


@api_v1.route("/series/<string:series_id>", methods=["GET"])
@authentication_required()
def get_single_series(series_id):
    """Get a game series"""
    series = get_series_by_id(series_id)
    return make_success_response(response_data=series.as_json())


@api_v1.route("/series", methods=["PUT"])
@authentication_required()
@request_data_required(*["name", "start_date", "end_date"])
def add_series():
    """Add a game series"""
    series = create_series(
        name=request.json["name"],
        start_date=request.json["start_date"],
        end_date=request.json["end_date"]
    )
    return make_success_response(response_data=series.as_json())


@api_v1.route("/series/<string:series_id>", methods=["POST"])
@authentication_required()
@request_data_required(*["name", "start_date", "end_date"])
def update_series(series_id):
    """Update a game series"""
    series = update_game_series(
        series_id=series_id,
        name=request.json["name"],
        start_date=request.json["start_date"],
        end_date=request.json["end_date"]
    )
    return make_success_response(response_data=series.as_json())


@api_v1.route("/series/<string:series_id>", methods=["DELETE"])
@authentication_required()
def delete_series(series_id):
    """Delete a game series"""
    series = get_series_by_id(series_id)
    series.delete()

    return make_success_response(response_data=None)
