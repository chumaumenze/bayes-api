import os
from uuid import uuid4

from application import constants, errors


def generate_unique_reference(numeric=False, size=64):
    return uuid4().int >> 128 - (128 - size) if numeric else uuid4().hex[:size]


def detect_configuration_mode():
    config_mode = os.environ["FLASK_ENV"]
    if config_mode in constants.ALLOWED_CONFIGURATION_MODES:
        print(f"'{config_mode}' configuration mode detected.")  # noqa
        return config_mode

    error_message = (f"Invalid or no configuration mode ('{config_mode}') "
                     f"detected! Aborting...")
    raise errors.ConfigNotFound(error_message)
