from flask import jsonify, make_response


def _make_api_response(response_data, http_code):
    return make_response(jsonify(response_data), http_code)


def make_failure_response(http_code, message):

    response_data = {
        "status": "FAILURE",
        "error": {
            "code": http_code,
            "message": message
        }
    }

    return _make_api_response(response_data, http_code)


def make_success_response(response_data, http_code=200):

    response_data = {
        "status": "SUCCESS",
        "data": response_data
    }

    return _make_api_response(response_data, http_code)
