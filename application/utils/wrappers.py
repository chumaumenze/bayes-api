from functools import wraps

from flask import g

from application import errors
from application.utils.logger import logger


def request_data_required(f):
    """Check if request body contains [valid] request data"""

    @wraps(f)
    def decorated_func(*args, **kwargs):
        if not g.request_data:
            logger.error("Invalid/No request data!")
            raise errors.InvalidRequestData
        return f(*args, **kwargs)

    return decorated_func


def is_user_enabled(f):
    """Check if current user is enabled/disabled

    Will prevent disabled users from gaining access.
    """

    @wraps(f)
    def decorated_func(*args, **kwargs):
        logger.debug(f.func_name)  # DEBUG
        logger.debug("Ensuring user is enabled...")
        try:
            if g.user.status_id != 1:
                raise errors.APIError
            logger.debug("Authorized user granted access.")
        except errors.APIError:
            logger.error("Unauthorized user denied access!")
            raise errors.UnauthorizedUser
        return f(*args, **kwargs)

    return decorated_func
