import json
from datetime import datetime

from sqlalchemy.exc import SQLAlchemyError
from werkzeug.security import check_password_hash, generate_password_hash

from application import db, errors
from application.constants import (
    VERY_SHORT_TEXT_LENGTH, SHORT_TEXT_LENGTH,
    TINY_TEXT_LENGTH, LONG_TEXT_LENGTH, VERY_LONG_TEXT_LENGTH)
from application.contexts import current_request_time, current_user_id
from application.utils.logger import logger
from application.utils.helpers import generate_unique_reference


def _handle_db_commit_error(error_msg, error_to_raise=None):
    logger.error(error_msg, exc_info=True)
    db.session.rollback()
    if error_to_raise is not None:
        raise error_to_raise()


def _commit_to_db(error_msg, error_to_raise=None):
    try:
        db.session.commit()
    except SQLAlchemyError:
        _handle_db_commit_error(error_msg, error_to_raise)


class BaseModel(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, default=datetime.now)
    modified_at = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean, default=True)

    def save(self, _commit=True, _error_msg=None, _raise=errors.APIError):
        """Save a model

        :param bool _commit: Allow/disallow immediate commit
        :param str _error_msg: Error message in case of exception
        :param object _raise: Exception to raise in case of error
        :return:
        """
        db.session.add(self)
        if _commit:
            _commit_to_db(
                error_msg=_error_msg or "Error persisting item <{0}> "
                                        "to database!"
                                        "".format(self.__class__.__name__),
                error_to_raise=_raise
            )

    def delete(self, _commit=True, _error_msg=None, _raise=errors.APIError):
        """Delete a model

        :param bool _commit: Allow/disallow immediate commit
        :param str _error_msg: Error message in case of exception
        :param object _raise: Exception to raise in case of error
        :return: None
        :rtype: None
        """
        db.session.delete(self)
        if _commit:
            _commit_to_db(
                error_msg=_error_msg or "Error deleting item <{0}> "
                                        "from database!"
                                        "".format(self.__class__.__name__),
                error_to_raise=_raise
            )

    def update(self, _commit=True, _error_msg=None, _raise=errors.APIError,
               **kwargs):
        """Update a model

        :param bool _commit: Allow/disallow immediate commit
        :param str _error_msg: Error message in case of exception
        :param object _raise: Exception to raise
        :param kwargs: Key-value pair values to update
        :return: None
        :rtype: None
        """

        for k, v in kwargs.items():
            setattr(self, k, v)
        if _commit:
            _commit_to_db(
                error_msg=_error_msg or "Error updating item <{0}> "
                                        "in database!"
                                        "".format(self.__class__.__name__),
                error_to_raise=_raise
            )

    @classmethod
    def get(cls, _desc=True, **params):
        """Get value from current model

        :param bool _desc: Query in descending order
        :param params: Key-value query filters
        :return: SQLAlchemy object of the first matched item
        """
        if _desc:
            return (
                cls.query.order_by(cls.id.desc()).filter_by(**params).first()
            )
        return cls.query.filter_by(**params).first()


class Series(BaseModel):
    __tablename__ = "series"

    name = db.Column(db.String(VERY_SHORT_TEXT_LENGTH), nullable=False)
    start_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)

    @property
    def is_active(self):
        return self.end_date > datetime.now()

    def as_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "start_date": self.start_date,
            "end_date": self.end_date,
            "is_active": self.is_active
        }


class Tournament(BaseModel):
    __tablename__ = "tournaments"

    name = db.Column(db.String(VERY_SHORT_TEXT_LENGTH), nullable=False)
    description = db.Column(db.String(LONG_TEXT_LENGTH))
    start_date = db.Column(db.DateTime, nullable=False)
    end_date = db.Column(db.DateTime, nullable=False)
    series_id = db.Column(db.Integer, db.ForeignKey("series.id"), nullable=False)
    city = db.Column(db.String(SHORT_TEXT_LENGTH), nullable=False)
    country = db.Column(db.String(SHORT_TEXT_LENGTH), nullable=False)

    series = db.relationship("Series", backref=db.backref("tournaments", uselist=True))

    def is_active(self):
        return self.end_date > datetime.now()

    def as_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "start_date": self.start_date,
            "end_date": self.end_date,
            "city": self.city,
            "country": self.country,
            "series": self.series.as_json(),
            "is_active": self.is_active()
        }


class User(BaseModel):
    __tablename__ = "users"

    first_name = db.Column(db.String(VERY_SHORT_TEXT_LENGTH))
    last_name = db.Column(db.String(VERY_SHORT_TEXT_LENGTH))
    email = db.Column(db.String(VERY_SHORT_TEXT_LENGTH), nullable=False,
                      unique=True, index=True)
    address = db.Column(db.String(VERY_SHORT_TEXT_LENGTH))
    username = db.Column(db.String(VERY_SHORT_TEXT_LENGTH), nullable=False,
                         unique=True, index=True)
    password = db.Column(db.Text, nullable=False)
    api_key = db.Column(db.String(VERY_SHORT_TEXT_LENGTH), unique=True,
                        nullable=False, default=generate_unique_reference)

    def as_json(self):
        return {
            "user_id": self.id,
            "username": self.username,
            "address": self.address,
            "email": self.email,
            "first_name": self.first_name,
            "last_mame": self.last_name
            # "api_key": self.api_key
        }

    def __setattr__(self, key, value):
        if key == "password":
            value = generate_password_hash(value)

        super().__setattr__(key, value)

    def verify_password(self, password):
        return check_password_hash(self.password, password)
