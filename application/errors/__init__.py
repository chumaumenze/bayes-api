# HTTP error codes are specified at registration point
# i.e. app.register_error_handler([error_code], [error_handle_name_here])

_DEFAULT_ERROR_CODE = 500  # internal server error
_DEFAULT_ERROR_MESSAGE = (
    "Server cannot validate requests sent at this time, please try again."
)


class APIError(Exception):
    """Base exception for all exceptions"""

    code = _DEFAULT_ERROR_CODE
    message = _DEFAULT_ERROR_MESSAGE

    def __init__(self, message=None, code=None, log_message=None,
                 _auto_raise=False):  # TODO: Make auto_raise `True`
        self.message = (message or getattr(self.__class__, "message",
                                           _DEFAULT_ERROR_MESSAGE))
        self.code = (code or getattr(self.__class__, "code",
                                     _DEFAULT_ERROR_CODE))
        self.log_message = log_message or self.message

        if _auto_raise:
            raise self


class ConfigNotFound(Exception):
    code = 500
    message = "Configuration mode not found."


class InvalidRequestData(APIError):
    code = 400  # bad request
    message = "Either request sent has no payload data, or some " \
              "parameters were missing, incomplete or not properly formatted."


class BadRequest(APIError):
    code = 400
    message = "Request was not properly formatted."


class ResourceNotFound(APIError):
    code = 404  # not found
    message = "Cannot find the resource requested for."


class UnsuccessfulAuthentication(APIError):
    code = 401  # unauthorized; failed authentication
    message = "Unsuccessful authentication. Username and/or password invalid."


class UnauthorizedUser(APIError):
    code = 401  # unauthorized; disabled user
    message = "User is not authorized to access the requested resource."


class UserNotFound(APIError):
    code = 401
    message = "User not found!"


class UserAlreadyExist(APIError):
    code = 401
    message = "User already exist!"


class Forbidden(APIError):
    code = 403
    message = "Forbidden!"


class InvalidToken(APIError):
    code = 401  # invalid token
    message = "Invalid authenticatoin token."


class TokenExpired(APIError):
    code = 498  # expired session
    message = "Your session is expired. Please re-authenticate."
