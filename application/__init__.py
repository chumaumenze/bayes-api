import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_jwt_extended import JWTManager

from application.utils import detect_configuration_mode
from application.utils.logger import setup_logging

BASE_DIR = os.path.dirname(os.path.abspath(__name__))
db = SQLAlchemy()
jwt = JWTManager()


def setup_core_tools(app):
    from application.errors.handlers import setup_error_handling

    setup_logging(app)
    db.init_app(app)
    jwt.init_app(app)

    setup_error_handling(app)

def _setup_blueprints(app):
    from application.controllers.api_v1 import api_v1
    from application.contexts.handlers import before_every_request
    from application.contexts.handlers import after_every_request

    api_v1_prefix = app.config.get("API_V1_PREFIX") or "/api/v1"

    app.register_blueprint(api_v1, url_prefix=api_v1_prefix)

    # Bind before request context handlers to blueprints
    app.before_request_funcs.update({
        api_v1.name: [before_every_request],
    })

    # Bind after request context handlers to blueprints
    app.after_request_funcs.update({
        api_v1.name: [after_every_request],
    })


def create_app():
    """Create a Flask application instance."""

    # Create application instance
    detect_configuration_mode()
    app = Flask(__name__, instance_relative_config=True)

    # Load the configuration settings
    app.config.from_pyfile("config.py")

    setup_core_tools(app)
    _setup_blueprints(app)

    return app
